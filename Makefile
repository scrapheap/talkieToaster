BABEL=./node_modules/babel-cli/bin/babel.js
BROWSERIFY=./node_modules/browserify/bin/cmd.js

MOCHA=./node_modules/mocha/bin/mocha
MOCHA_OPTIONS=--compilers js:babel-core/register --require babel-polyfill --ui=tdd

SASS=sass
SASS_OPTIONS=--style=compressed

.PHONY: all
all: html/thirdParty.js html/dialog-polyfill.css test js css


.PHONY: test
test:
	$(MOCHA) $(MOCHA_OPTIONS)


html/thirdParty.js: node_modules/babel-polyfill/dist/polyfill.min.js thirdParty/jquery-3.3.1.min.js
	cat $^ > $@


.PHONY: js
js: html/talkieToaster.js html/Favourites.js

html/talkieToaster.js: src/talkieToaster.js
	$(BROWSERIFY) $^ -t babelify --outfile $@

html/%.js: src/%.js
	$(BABEL) $^ > $@


.PHONY: css
css: html/style.css

html/%.css: scss/%.scss
	$(SASS) $(SASS_OPTIONS) $^ > $@

