function Favourites(options = {}) {
    var self = this;
    var favouritesList = {};
    var maxRank = 0;

    options.hooks = options.hooks || {};

    self.list = function () {
        var entries = [];

        for( var favourite in favouritesList ) {
            if (favouritesList.hasOwnProperty(favourite)) {
                entries.push(favouritesList[favourite]);
            }
        }

        return entries.sort((a, b) => {return a.rank - b.rank});
    };


    self.has = function (sentence) {
        var normalizedSentence = self.normalizeSentence(sentence);
        return favouritesList.hasOwnProperty(normalizedSentence);
    };


    self.add = function (favourite) {
        favourite.normalizedSentence = favourite.normalizedSentence || self.normalizeSentence(favourite.sentence);
        favourite.rank = favourite.rank || maxRank++;

        maxRank = Math.max(maxRank, favourite.rank + 1);

        favouritesList[favourite.normalizedSentence] = favourite;

        if (options.hooks.add) {
            options.hooks.add(favourite);
        }

        return favourite;
    };

    self.delete = function (sentence) {
        var normalizedSentence = self.normalizeSentence(sentence);

        if (options.hooks.delete && favouritesList[normalizedSentence]) {
            options.hooks.delete(favouritesList[normalizedSentence]);
        }

        delete favouritesList[normalizedSentence];
    };

    self.normalizeSentence = function (sentence) {
        var normalizedSentence = sentence.toLowerCase().replace(/\W/g, "");

        return normalizedSentence;
    }
}

module.exports = Favourites;
