var Favourites = require('./Favourites');
var synth = window.speechSynthesis;
var config = {
    voice: "Microsoft Hazel Desktop - English (Great Britain)",
    volume: 1,
    rate: 1,
    pitch: 1,
    sayOnEnter: true
};

var availableVoices = {};

var favourites = new Favourites(
    {
        hooks: {
            add: function (favourite) {
                var sentence = favourite.sentence
                var favouriteEntry = $('<li>')
                    .attr('data-normalizedSentence', favourite.normalizedSentence);

                var status = favourites.has(sentence) ? 'yes': 'no';

                var favouriteStatus = $('<span class="favouriteStatus"></span>')
                    .attr('data-sentence', sentence)
                    .attr('data-favourite', status)
                    .click(toggleFavourite);

                favouriteEntry.append(favouriteStatus);

                var loadSentence = $('<span class="sentence"></span>')
                    .attr('data-sentence', sentence)
                    .text(sentence)
                    .click(loadFromHistory);

                favouriteEntry.append(loadSentence);


                var sayNow = $('<span class="say"></span>')
                    .attr('data-sentence', sentence)
                    .click(function (e) {
                        var sentence = $(this).attr('data-sentence');
                        say(sentence);
                    });

                favouriteEntry.append(sayNow);
                $('#favouritesList').append(favouriteEntry);

                saveFavourites();
            },

            delete: function (favourite) {
                $('span.favouriteStatus[data-normalizedSentence=' + favourite.normalizedSentence + ']')
                    .attr('data-favourite', 'no');

                $('#favouritesList [data-normalizedSentence=' + favourite.normalizedSentence + ']')
                    .slideUp(200, function () {
                        $(this).remove();
                    });

                saveFavourites();
            }
        }
    }
);


$(document).ready(function () {
    $('dialog').each(function (index, dialog) {
        dialogPolyfill.registerDialog(dialog);
    });

    loadConfig();
    if (loadFavourites()) {
        $('article, footer').attr('data-active-tab', 'favourites');
    };

    $('button#say').click(function () {
        var sentence = $('#sentence').text();
        say(sentence);
        $('#sentence').text('');
    });

    $('#sentence').keypress( function(e) {
        if (config.sayOnEnter && e.keyCode == 13) {
            var sentence = $('#sentence').text();
            say(sentence);
            $('#sentence').text('');
            e.preventDefault();
            return false;
        }
    });

    $('span[data-tab]').click(function () {
        var tab = $(this).attr('data-tab');
        $('article').attr('data-active-tab', tab);
        $('footer').attr('data-active-tab', tab);
    });

    $('#settings').click(function () {
        $('#configuration')[0].showModal();
    });

    var voices = $('#voice');
    var volume = $('#volume');
    var rate = $('#rate');
    var pitch = $('#pitch');
    var sayOnEnter = $('#sayOnEnter');

    volume.val(config.volume || 1);
    rate.val(config.rate || 1);
    pitch.val(config.pitch || 1);
    sayOnEnter.prop('checked', config.sayOnEnter || false);

    voices.empty();

    function populateVoiceList() {
        synth.getVoices().forEach((voice) => {
            availableVoices[voice.name] = voice;
            var option = $('<option></option>');
            option.attr('value', voice.name);
            if ((!config.voice && voice.default) || (config.voice == voice.name)) {
                option.attr('selected', 'selected');
            }
            option.text(voice.name);
            voices.append(option);
        });
    }

    populateVoiceList();
    if (typeof speechSynthesis !== 'undefined' && speechSynthesis.onvoiceschanged !== undefined) {
        speechSynthesis.onvoiceschanged = populateVoiceList;
    }

    voices.change(function () {
        config.voice = voices.val();
    });

    volume.change(function () {
        config.volume = volume.val();
    });

    rate.change(function () {
        config.rate = rate.val();
    });

    pitch.change(function () {
        config.pitch = pitch.val();
    });

    sayOnEnter.change(function () {
        config.sayOnEnter = sayOnEnter.prop('checked');
    });

    $('button#closeSettings').click(function () {
        saveConfig();
        $('#configuration')[0].close();
    });
});



function loadConfig() {
    if (localStorage.config) {
        config = JSON.parse(localStorage.config);
    }
}


function loadFavourites() {
    var storedFavourites = JSON.parse(localStorage.favourites || "[]");
    storedFavourites.forEach((sentence) => favourites.add({sentence}));

    return storedFavourites.length
}


function saveFavourites() {
    localStorage.favourites = JSON.stringify(
        favourites.list().map((favourite) => favourite.sentence)
    );
}


function saveConfig() {
    localStorage.config = JSON.stringify(config);
}


function say(sentence) {
    appendToHistory(sentence);

    var utterance = new SpeechSynthesisUtterance(sentence);

    if(availableVoices[config.voice]) {
        utterance.voice = availableVoices[config.voice];
    }
    utterance.pitch = config.pitch;
    utterance.rate = config.rate;
    utterance.volume = config.volume;

    synth.speak(utterance);
}


function toggleFavourite(e) {
    var sentence = $(this).attr('data-sentence');

    if (favourites.has(sentence)) {
        favourites.delete(sentence);
        $(this).attr('data-favourite', 'no');
    } else {
        var favourite = favourites.add({sentence});
        $(this).attr('data-favourite', 'yes');
    }
}


function appendToHistory(sentence) {
    var historyEntry = $('<li>');

    var status = favourites.has(sentence) ? 'yes': 'no';

    var favouriteStatus = $('<span class="favouriteStatus"></span>')
        .attr('data-sentence', sentence)
        .attr('data-normalizedSentence', favourites.normalizeSentence(sentence))
        .attr('data-favourite', status)
        .click(toggleFavourite);

    historyEntry.append(favouriteStatus);

    var loadSentence = $('<span class="sentence"></span>')
        .attr('data-sentence', sentence)
        .text(sentence)
        .click(loadFromHistory);

    historyEntry.append(loadSentence);


    var sayNow = $('<span class="say"></span>')
        .attr('data-sentence', sentence)
        .click(function (e) {
            var sentence = $(this).attr('data-sentence');
            say(sentence);
        });

    historyEntry.append(sayNow);
    $('#historyList').append(historyEntry);
}


function loadFromHistory(e) {
    var sentence = $(this).attr('data-sentence');
    $('#sentence').text(sentence).focus();
    setEndOfContenteditable($('#sentence')[0]);
}


function setEndOfContenteditable(contentEditableElement) {
    var range,selection;
    if (document.createRange) {
        range = document.createRange();
        range.selectNodeContents(contentEditableElement);
        range.collapse(false);
        selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);

    } else if (document.selection) { 
        range = document.body.createTextRange();
        range.moveToElementText(contentEditableElement);
        range.collapse(false);
        range.select();
    }
}
