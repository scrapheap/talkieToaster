var path = require('path');
var assert = require('chai').assert;

var Favourites = require(path.join(__dirname, '..', 'src/Favourites'));


suite("Favourites", function () {
    test("methods", function () {
        var favourites = new Favourites();

        var favourite = {
            sentence: "This is a test"
        };

        var expectedFavourite = {
            sentence: "This is a test",
            normalizedSentence: "thisisatest",
            rank: 0
        };

        assert.deepEqual(favourites.list(), [], "list for a new instance should return an empty list");

        assert.deepEqual(favourites.add(favourite), expectedFavourite, "add should return the processed favourite entry");

        assert.deepEqual(favourites.list(), [ expectedFavourite ], "list after adding should return somethig");

        var favourite2 = {
            sentence: "This is a test",
            rank: 10
        };

        var expectedFavourite2 = {
            sentence: "This is a test",
            normalizedSentence: "thisisatest",
            rank: 10
        }

        favourites.add(favourite2);

        assert.deepEqual(favourites.list(), [ expectedFavourite2 ],  "duplicate add should update the existing favourite entry");

        var favourite3 = {
            sentence: "So is this",
        };

        var expectedFavourite3 = {
            sentence: "So is this",
            normalizedSentence: "soisthis",
            rank: 11
        };

        favourites.add(favourite3);

        assert.deepEqual(favourites.list(), [ expectedFavourite2, expectedFavourite3 ],  "auto rank should always be one higher than current max rank seen");

        var favourite4 = {
            sentence: "This is a test of lower ranking",
            rank: 2
        };

        var expectedFavourite4 = {
            sentence: "This is a test of lower ranking",
            normalizedSentence: "thisisatestoflowerranking",
            rank: 2
        }

        favourites.add(favourite4);

        assert.deepEqual(favourites.list(), [ expectedFavourite4, expectedFavourite2, expectedFavourite3 ],  "list order of favourites should be in rank order");

        favourites.delete(expectedFavourite3.sentence);

        assert.deepEqual(favourites.list(), [ expectedFavourite4, expectedFavourite2 ],  "delete method should remove the entry from the list");

        assert.equal(favourites.has(expectedFavourite4.sentence), true, "has should return true if the favourite is in the list");
        assert.equal(favourites.has(expectedFavourite3.sentence), false, "has should return false if the favourite is not in the list");
    });

    test("hooks", function () {
        var lastAddedFavourite;
        var lastDeletedFavourite;
        var favourites = new Favourites(
            {
                hooks: {
                    add: function (favourite) {
                        lastAddedFavourite = favourite;
                    },

                    delete: function (favourite) {
                        lastDeletedFavourite = favourite;
                    }
                }
            }
        );

        var favourite = {
            sentence: "This is a test"
        };

        var expectedFavourite = {
            sentence: "This is a test",
            normalizedSentence: "thisisatest",
            rank: 0
        };

        favourites.add(favourite);
        assert.deepEqual(lastAddedFavourite, expectedFavourite, "the add method should run the add hook if present");

       
        lastSeenFavourite = undefined;
        favourites.delete(favourite.sentence);
        assert.deepEqual(lastDeletedFavourite, expectedFavourite, "the delete method should run the delete hook if present");
    });
});
